export default {
  en: {
    form: {
      firstName: "First name",
      lastName: "Last name",
      email: "Email",
      mobile:"Mobile",
      password:"Password",
      confirmPassword:"Confirm Password",
      terms: "I accept the terms and conditions",
      type: "Select Age type",
      image:"Image",
      additionalInfo: "Additional info",
      fileupload: "Select an Image",
      submitted: "The form is submitted!",
      sentInfo: "Here is the info you sent:",
      return: "Return to the form",
      submit: "Submit",
      submitting: "Submitting",
      charactersLeft: "You have {charCount} character left. | You have {charCount} characters left.",
      types: {
        free: "Teen",
        starter: "Adult",
        enterprise: "Elderly"
      }
    },
    error: {
      invalidFields: "Following fields have an invalid or a missing value:",
      general: "An error happened during submit.",
      generalMessage: "Form sending failed due to technical problems. Try again later.",
      fieldRequired: "{field} is required.",
      fieldInvalid: "{field} is invalid or missing.",
      numberValid:"{field} must have 10digits",
      image:"{field} is required",
      FieldExpression:"{field}  must contain a letter, a number, a special character, and be more than 8 characters long.",
      confirmPassword:"{field} doesn't match" ,
      fieldMaxLength: "{field} maximum characters exceeded."
    }
  }
};
